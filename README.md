Goal of the project: To create a webpage which consist of 3-4 pages of a survey template to figure out which kind of outing NOC team would like to have. At the final page we will get results of the survey with a request for outing. Webpage structure will be decided based on the first package task which will contain the main HTML and CSS pages and design,Layout for the webpage.

This project will start by applying certain packages, these packages will be assigned to each one of you:

Package # 1:

1- Collect data
2- Choose the website layout
3- design the survey pages using (html, css and bootstrap)

@dabbas1 Please work with Nisreen and @aabdullah8 for details

Package # 2:

1- lunch ec2 instance
2- install node, express and mongo
3- generate the project template with express
4- push the main project to bitbucket

@salqsous please work with @aabdullah8 for details

Package # 3:

1- design the login page
2- design the result page
3- merge the code with express template

@nfraihat please work with @aabdullah8 for details

Package # 4:

Take care of the backend code related to the login page

@aalmonayer please work with @aabdullah8 for details

Package # 5:

Take care of the backend code related to the survey pages and data collection

@aabdullah8

Package # 6 :

Take care of the backend code related to the result page

@ookour please work with @aabdullah8 for details

Package # 7:

Merge the project and fix the backend errors after

@malmohtasib Please work with @aabdullah8 for details and deploy to server.